
<html>
    <head>
        <meta charset="UTF-8">
        <title>MiniApp</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/style.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>public/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/jquery-2.1.4.min.js"></script>
    </head>
    <body>
        <script type="text/javascript">

$(document).ready(function(){
	$("#frm").validate({
		rules: {
			number	:	"required"
		},
		messages: {
			number	:	"Kindly enter first name"
		},
	});
});

</script>
        
    <div class="wrapper"> 
        <div class="container">
            <!--<h1>Register</h1>-->
            <?php if(isset($error)){ echo $error ;} ?>
            <form class="form" name="frm" method="POST" action="<?php echo base_url('register/validate_credentials'); ?>">
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="number" name="number" placeholder="Enter Number" value="<?php echo set_value('number'); ?>">
                      <?php echo form_error('number'); ?> 
                </div>
                <br>
                <input type="submit" name="register" value="Register">
            </form>

        </div>

        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li><li></li>
            
        </ul>
    </div>
<!--    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="js/index.js"></script>-->
    </body>
</html>
<script src="<?php echo base_url(); ?>/public/js/index.js"></script>
