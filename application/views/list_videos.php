<!DOCTYPE html>
<html>
<?php include_once "header.php"; ?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Tables
            <small>advanced tables</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Video Id</th>
                        <th>Uploader Name</th>
                        <th width="50px">Video Url</th>
                        <th>Splash Url</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($videos_data as $key => $value) { ?>
                    <tr>                                 
                        <td><input name="type" type="checkbox" class="checkbox" value="<?php echo $value->ID ?>" /></td>
                        <td><?php echo $value->ID ?></td>
                        <td><?php echo $value->uploader_name ?></td>
                        <td class="linebreak">
                         <!--  <video  width="465" height="315" controls autoplay>
                            <source src="<?php echo $value->video_url?>" type="video/mp4">
                          </video> -->
                          <a href="<?php echo $value->video_url?>"><?php echo $value->video_url?></a>
                        </td>
                        <td><img src="<?php echo $value->splash_url ?>" class="splash mage" alt="User Image" height="50px" width="50px"></td>
                        <td><a  class="btn btn-app" href="<?php echo base_url('update/edit'); ?>/<?php echo $value->ID ?>"> <i class="fa fa-edit"></i></a></td>
                        <td><input type="button" id="delete" class="btn btn-block btn-primary" name="Delete" value="Delete" onclick="verify(<?php echo $value->ID ?>)"></td>
                    </tr>
                    <?php
                        }
                     ?>
                     
                    </tbody>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th>Video Id</th>
                        <th>Uploader Name</th>
                        <th>Video Url</th>
                        <th>Splash Url</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </tfoot>
                    <tr><td colspan="6"><a href="javascript:;" class="deleteall" title="dtable">Delete Selected</a></td></tr>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>
        </div>
        <strong>Copyright &copy; 2015-2016 <a href="#">Miniapp</a>.</strong> All rights reserved.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->

      <?php include_once "footer.php"; ?>

    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>public/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src=".<?php echo base_url(); ?>public/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>public/lugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>public/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>public/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>
<script type="text/javascript">

    var videosIds = [];
    function verify(e) {
     alert(e);
        base_url = '<?php echo base_url(); ?>';
        $.ajax({
            type: "POST",
            url: base_url + "update/delete",
            data: {
                rule_id: e
            },
            dataType: "text",
            cache: false,
            success:
                    function (data) {
                     console.log(data);
                        //  alert(data);  //as a debugging message.
                        if (data == '1') {
                            
                            $('#msg').html('<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Success!</strong> Video Deleted</div>');
                            window.setTimeout(function () {
                                location.reload()
                            }, 500)
                        }
                    }
        });// you have missed this bracket
        return false;

    }
      
  $('.deleteall').on("click", function(event){
        videosIds = [];
          $("input:checkbox[name=type]:checked").each(function(){
          videosIds.push($(this).val());
      });
      console.log(videosIds);
    if(videosIds.length > 0 ){
      base_url = '<?php echo base_url(); ?>';
        $.ajax({
            type: "POST",
            url: base_url + "update/deleteall",
            data: {
                rule_id: videosIds
            },
            dataType: "text",
            cache: false,
            success:
                    function (data) {
                      console.log(data);
                                location.reload();
                        
                    }
        });     
    }else{
      alert('No Record Selected');
    }           
  });



</script>




