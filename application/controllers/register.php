<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('verify_m');
        $this->load->model('login_m');
    }

    public function index() {
        $this->load->view('register_s2');
    }

    public function resendCode() {

        //$mob_numb = json_decode($this->post('number'), true);
        $mob_numb = $this->input->post('number');
        if(isset($mob_numb) && $mob_numb != ''){
        $trigger_value = '1';

        //generate randon 4 digit number
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        //check count for trigger value
        $count_result = $this->verify_m->get_count($mob_numb, $trigger_value);

        //uncomment this
        if ($count_result == 3) {
            $response = 'Limit exceed';
        } else {

            //call to pringit api to send message
            $url = "http://www.pringit.com/api/?username=mini&action=PM&secret=44c6c370fd1859325f7119e96a81584e&v=1&mobile=$mob_numb&keyword=mini&message=$random_number&footer=0";
            $response_server = $this->httpGetWithErros($url);


            if ($response_server == 'Message sent') {
                $data_result = $this->verify_m->update_code($mob_numb, $random_number, $trigger_value);
                print_r($data_result);
            } elseif ($response_server == "Mobile number is not valid") {
                $message_response = "Not Valid";
                print_r($message_response);
            } elseif ($response_server == "internet problem") {
                $message_response = "No Internet";
                print_r($message_response);
            } else {
                $response = 'issue in sending sms';
                print_r($response);
            }
        }
        }
    }

    public function register_user() {
     
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data = $this->input->post();
            $data['error'] = $data;
            $this->load->view('register_s2', $data);
        } else {

            $user_data = $this->input->post();
            $data_result = $this->login_m->register_online_user($user_data);
            // $this->session->unset_userdata('id');
            $data['message'] = 'you have registered successfully';
            $this->load->view('login');
        }
    }

    public function validate_credentials() {
        $this->form_validation->set_rules('number', 'Number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('register_v');
        } else {
            
            
            $mob_numb = $this->input->post('number');
            if(isset($mob_numb) && $mob_numb != ''){
            $user_number = array('id' => $mob_numb);
            $this->session->set_userdata($user_number);
            
            //check if number already exists
            $number_exits = $this->verify_m->number_exists($mob_numb);
          
            if ($number_exits != '1') {
                
               
                $number_exits = $this->verify_m->get_code_online($mob_numb);
                if($number_exits != ''){
                    if(isset($number_exits[0]->verify_code)){
                $random = $number_exits[0]->verify_code;
                    }else{
                        
                        $random = '';
                    }
                
                    }
                //generate randon 4 digit number
                if($random == ''){
                $digits = 4;
                $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
                //call to pringit api to send message
                //  $url = "http://www.pringit.com/api/?username=mini&action=PM&secret=44c6c370fd1859325f7119e96a81584e&v=1&mobile=$mob_numb&keyword=mini&message=$random_number&footer=0";
                ///  $response_server = $this->httpGetWithErros($url);
              
                //  print_r($response_server);
                //   if ($response_server == 'Message sent') {
                $data['user'] = $this->verify_m->insert_code_online($mob_numb, $random_number);
                $this->load->view('register_s2', $data);
                  }else{ 
                $random_number = $random;
                $data['user'] = $this->verify_m->get_code_online($mob_numb);
                $this->load->view('register_s2', $data);
                }
            
                
            } else {

                //$data['error'] = 'Number already exists';
                $number_exits = $this->verify_m->get_code_online($mob_numb);
                $random_number = $number_exits[0]->verify_code;
//                print_r($number_exits);
//                exit;
                $data['user'] = $this->verify_m->insert_code_online($mob_numb, $random_number);
                $this->load->view('register_s2',$data);
            }
        }
        
        }
    }
    
    public function update() {
      
        $session_id = $this->session->all_userdata();
        $id = $session_id[0]->user_id;
        $get_update_data = $this->verify_m->number_exists($mob_numb);
        $this->load->view('update');
        
    }

    //Function to connect with pringit api//
    public function httpGetWithErros($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        if ($output === false) {
            echo "Error Number:" . curl_errno($ch) . "<br>";
            echo "Error String:" . curl_error($ch);
            $output = "internet problem";
            return $output;
            //	exit();
        }
        curl_close($ch);

        return $output;
    }

}

?>