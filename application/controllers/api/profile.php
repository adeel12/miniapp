<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Profile extends REST_Controller {

    function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->load->model('profile_m');
        $this->load->library('my_phpmailer');
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload');
    }

    function updateData_post() { 

 
        $profile_data = json_decode(file_get_contents('php://input'));
        if($profile_data->email == null){
            $msg = array('Err' => 'Email is missing');
            $this->response($msg);
        } 
        if($profile_data->cover_image != null && $profile_data->image != null){ 
            $digits = 4;
            $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
            $image_name = 'img_p_';
            // $destination_folder = $_SERVER['DOCUMENT_ROOT'].'/miniapp/upload/'.$image_name.'_'.$random_number.'.png';
            $destination_folder = 'http://localhost:8888//miniapp/upload/'.$image_name.'_'.$random_number.'.png';
            $data= $profile_data->cover_image; 
            // header('Content-Type: image/png');
            $data = base64_decode($data);
            file_put_contents($destination_folder, $data, 100);
            $cover_image = base_url().'upload/'.$image_name.'_'.$random_number.'.png'; 
            $result_data = $this->profile_m->update_user($cover_image , $profile_data);            
        }else if($profile_data->cover_image != null || $profile_data->cover_image != ''){
            $result_data = $this->profile_m->update_cover_image($profile_data); 
        }else if($profile_data->image != null){
            $result_data = $this->profile_m->update_profile_image($profile_data);     
        }else{

            $result_data = $this->profile_m->update($profile_data);  
        }


            if ($result_data == 'true') {
                $response = array('message' => 'update successfull');
                $this->response($response);

            } else {

                $response = array('message' => 'update not unsuccessfull');
                echo $this->response($response);
            }
    }

    public function test_post(){

        $profile_data = json_decode(file_get_contents('php://input'));
        print_r($profile_data);
        exit; 


    }

    function contacts_post() {

        $alldata = json_decode(file_get_contents('php://input'));
        if (!empty($alldata)) {
            $imported_data = $this->profile_m->contacts_list($alldata->contacts);
            print_r($imported_data);
        }
    }

    function videos_get() {

        $num = $this->get('id');
        $number = str_replace(' ', '+', $num);
        $videos_data = $this->profile_m->allvideos($number);
        // print_r($videos_data);
        $videos = array('awais' => $videos_data);
        $this->response($videos);
        //echo json_encode($videos, JSON_UNESCAPED_SLASHES), "\n";
    }

    function profileData_post() {



        $data = json_decode(file_get_contents('php://input'));
        $proflie_data = $this->profile_m->getUserData($data);
        //echo json_encode($proflie_data[0], JSON_UNESCAPED_SLASHES), "\n";
        echo $this->response($proflie_data[0]);
    }

    function miniuser_get() {



        $num = $this->get('number');
        $number = '+'.$num;
        $proflie_data = $this->profile_m->getMiniUser($number);
        $mini_data = array('mini' => $proflie_data);
        $this->response($mini_data);
    }

    function nonMiniuser_get() {

        $email = $this->get('email');
        $nonMini_data = $this->profile_m->getNonMiniUser($email);
        $non_mini_data = array('non_mini' => $nonMini_data);
        echo json_encode($non_mini_data);

    }

    function notification_post() {

        $alldata = json_decode(file_get_contents('php://input'));

        foreach ($alldata->contacts as $value) {
        $message = "notification";
        $mob_numb = '+923129436067';
        $url = "http://www.pringit.com/api/?username=mini&action=PM&secret=44c6c370fd1859325f7119e96a81584e&v=1&mobile=$mob_numb&keyword=mini&message=$message&footer=0";
        $response_server = $this->httpGetWithErros($url);
        }
        print_r($response_server);

    }

    function getSelectedContact_post() {

        $alldata = json_decode(file_get_contents('php://input'));
        if (!empty($alldata)) {
            $imported_data = $this->profile_m->insert_contact_list($alldata->contacts);
            $contacts = trim($imported_data, ", ");
            print_r($contacts);
        }
    }

    //get count of miniApp user for clan call
    function miniCount_get() {

        $num = $this->get('number');
        $number = str_replace(' ', '+', $num);
        $count_data = $this->profile_m->get_mini_count($number);
        print_r($count_data);
    }

    //forgot password functionality
    public function forgetPass_post(){

        $forget_data = json_decode(file_get_contents('php://input'));
        $email = $forget_data->email;
        $res = $this->profile_m->password($email);
        $message = $res->password;
        $response_server = $this->sendMail($email , $message);
        $this->response($response_server);

    }

    public function viewer_get(){
        $uid = $this->get('uid');
        $res = $this->profile_m->viewer_info($uid);
        if($res == null || ""){
            $data = array('err' => 'No data exists');
            $this->response($data, 404);            
        }
            $data = array('data' => $res);
            $this->response($data, 200);
    }    

    //Function to connect with pringit api//
    public function httpGetWithErros($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        if ($output === false) {
            echo "Error Number:" . curl_errno($ch) . "<br>";
            echo "Error String:" . curl_error($ch);
            $output = "internet problem";
            return $output;
        }
        curl_close($ch);
        return $output;
    }


    function sendMail($email , $random_number){

    //call model to update data in db
   if(!empty($email)){
      $message = "Your password is ". $random_number . ' Thanx for using miniapp';
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "testonebyte@gmail.com";  // user email address
        $mail->Password   = "1byte@biz";            // password in GMail
        $mail->SetFrom('testonebyte@gmail.com', 'Miniapp');  //Who is sending the email
        //$mail->AddReplyTo("response@yourdomain.com","Firstname Lastname");  //email address that receives the response
        $mail->Subject    = "Miniapp";
        $mail->Body      =  $message;
        $mail->AltBody    = "Plain text message"; 
        $destino = $email; // Who is addressed the email to
        $mail->AddAddress($destino, "Miniapp");

      //  $mail->AddAttachment("images/phpmailer.gif");      // some attached files
      //  $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $msg = "Error: " . $mail->ErrorInfo;
        } else {
            $msg = "Check your email for password";
        }

      return $msg;
     }


    } 

}
