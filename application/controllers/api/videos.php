<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Videos extends REST_Controller {

    function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->load->model('videos_m');
    }

    function share_post() {

        $data = json_decode(file_get_contents('php://input'));   
        
        // $response_data = $this->videos_m->video_share($data);
        // foreach ($response_data as $value) {
            
        //     $mob_numb = $value->number;
        //     //$mob_numb = '+923129436067';
        //     $message = 'Video';
        //     $string = str_replace(' ', '%20', $message);

        //     //call to pringit api to send message
        //     $url = "http://www.pringit.com/api/?username=mini&action=PM&secret=44c6c370fd1859325f7119e96a81584e&v=1&mobile=$mob_numb&keyword=mini&message=$message&footer=0";
        //     $response_server = $this->httpGetWithErros($url);
           
        //   print_r($response_server);
        //   exit;
        //     //$this->response($response_server);
        // }
        // $status = 'video share succesfully';
        
        //code for sendng push notification
        $gcm_user_data = $this->videos_m->get_gcm_users($data);
        define("GOOGLE_API_KEY", "AIzaSyBhMBMLm17R9mPBQ1Fyzz38CNcEG6qi9YM");
        foreach ($gcm_user_data as $value) {
            $registatoin_ids = $value->gcm_regid;
            $pushMessage = 'testing';
            $gcmRegIds = array("rid" => $registatoin_ids);
            $message = array("m" => $pushMessage);
            $response_server = $this->sendPushNotificationToGCM($registatoin_ids, $message);
            print_r($response_server);
            $registatoin_ids = '';
        }

       // $this->response($status);
    }

    //Function to connect with pringit api//
    public function httpGetWithErros($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        if ($output === false) {
            echo "Error Number:" . curl_errno($ch) . "<br>";
            echo "Error String:" . curl_error($ch);
            $output = "internet problem";
            return $output;
            //	exit();
        }
        curl_close($ch);

        return $output;
    }
    
    function test_post(){
        
        $data = json_decode(file_get_contents('php://input')); 
        $gcm_user_data = $this->videos_m->get_gcm_users($data);
        print_r($gcm_user_data);
        exit;
            $pushMessage = 'test';
            $registatoin_ids = 'APA91bGAV_7dse1jFBaW8-h5rtAdWp6JXkDVsVuO71fMbqO-IOu5BU5D044WKLMox0QLIib9FF09eZM-TB9tnt5pdH6GpS7Lca2H9P3wIPb-LDK2UszPJiIfatVEylATWQV4vkIJyOayFo3zEX7exCt5p8K6ctlNUA';
            $gcmRegIds = array("rid" => $registatoin_ids);
            $message = array("m" => $pushMessage);
            $response_server = $this->sendPushNotificationToGCM($registatoin_ids ,$message );
            print_r($response_server);
        
    }
    
    
    function sendPushNotificationToGCM($registatoin_ids, $message) {
		//Google cloud messaging GCM-API url
        $url =  'https://android.googleapis.com/gcm/send';
        
        
        $fields = array(
                'registration_ids'  => array($registatoin_ids),
                'data'              => array( "message" => $message ),
                );
        
		// Google Cloud Messaging GCM API Key
				
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    function sendMail($email , $random_number){

    //call model to update data in db
    if(!empty($email)){
      $message = "Your verification code is ". $random_number . 'Thanx for using miniapp';
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "testonebyte@gmail.com";  // user email address
        $mail->Password   = "1byte@biz";            // password in GMail
        $mail->SetFrom('testonebyte@gmail.com', 'Miniapp');  //Who is sending the email
        //$mail->AddReplyTo("response@yourdomain.com","Firstname Lastname");  //email address that receives the response
        $mail->Subject    = "Miniapp Verification Code";
        $mail->Body      =  $message;
        $mail->AltBody    = "Plain text message"; 
        $destino = $email; // Who is addressed the email to
        $mail->AddAddress($destino, "John Doe");

      //  $mail->AddAttachment("images/phpmailer.gif");      // some attached files
      //  $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $msg = "Error: " . $mail->ErrorInfo;
        } else {
            $msg = "200";
        }

      return $msg;
    }


    } 
    
}
