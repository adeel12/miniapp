<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller {

    function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->load->model('login_m');
    }

    function register_post() {

        $alldata = json_decode(file_get_contents('php://input'));
       
        //check number match with verify code
        $check_code = $this->login_m->check_code($alldata);
        if($check_code == '1'){
        $check_email = $this->login_m->email_exists($alldata);
        if($check_email == '1'){
           //exit('in check email funcion');
                $message = 'email already exists';
                $this->response($message, 406);
        }
        $response = $this->login_m->register($alldata);
            
            if($response == 'true') {
                $message = 'register';
                $this->response($message, 200);
            } else {
                $message = 'not';
                $this->response($message, 406);
            }
        }else {
            $message = 'verify code misMatch';
            $this->response($message, 406);
        }
    }

    function signin_post() {

        $signin_data = json_decode(file_get_contents('php://input'));
        if($signin_data->email != '' && $signin_data->password != ''){
        $result_data = $this->login_m->signin($signin_data);

        if ($result_data != '0') {
           $message = $result_data[0];
            $this->response($message, 200);
        } else {
            $message = array("mobile" => "not");
            $this->response($message, 406);
        }
        }else {
            
            $message = array("mobile" => "not");
            $this->response($message, 406);
        }
    }

}
