<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Gcm extends REST_Controller {

    function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->load->model('gcm_m');
    }

    function gcm_post() {

        $alldata = json_decode(file_get_contents('php://input'));
       
        //check number match with verify code
        $check_code = $this->gcm_m->check_code($alldata);
        print_r($check_code);
        
        if($check_code != '1'){
        $response = $this->gcm_m->register($alldata);
            
              if ($response == 'true') {
                $message = 'register';
                $this->response($message, 200);
            } else {
                $message = 'not';
                $this->response($message, 406);
            }
        }else {
            $message = 'verify code misMatch';
            $this->response($message, 406);
        }
        
    }
    
   //generic php function to send GCM push notification
    function sendPushNotificationToGCM($registatoin_ids, $message) {
        //Google cloud messaging GCM-API url
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
        // Google Cloud Messaging GCM API Key
        define("GOOGLE_API_KEY", "AIzaSyDA5dlLInMWVsJEUTIHV0u7maB82MCsZbU");
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}
