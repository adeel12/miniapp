<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package   CodeIgniter
 * @subpackage  Rest Server
 * @category  Controller
 * @author    Phil Sturgeon
 * @link    http://philsturgeon.co.uk/code/
 */
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Verify_number extends REST_Controller {

        function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->load->model('verify_m');
        $this->load->library('image_lib');
        $this->load->library('my_phpmailer'); 
       
    }

    function verify_post() {

        $mobile_data = json_decode(file_get_contents('php://input'));
        $email = $mobile_data->email;
        //check if number already exists
        $number_exits = $this->verify_m->number_exists($email); 
        if ($number_exits != 1) {
        //generate randon 4 digit number
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
        //call send email function
        $response_server = $this->sendMail($email , $random_number);
        if ($response_server !== '200') {
            $message_response = "Error in Sending Email";
            $this->response($message_response, 400);
        } 
        if($response_server == '200'){
            $data_result = $this->verify_m->insert_code($email, $random_number);
            $response = 'check your email for verification code';
            $this->response($response, 200);
        }
          }else{
        $response = 'Email already exists';
       $this->response($response,404);
      }
    }

    //function for resend code
    function resendCode_post() {
        
        $mobile_data = json_decode(file_get_contents('php://input'));
        $email  = $mobile_data->email;
        $trigger_value = $mobile_data->trigger_value;
        //generate randon 4 digit number
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        //check count for trigger value
        $count_result = $this->verify_m->get_count($email, $trigger_value);
        $response_server = $this->sendMail($email , $random_number);

        if ($response_server !== '200') {
            $message_response = "Error in Sending Email";
            $this->response($message_response, 400);
        } 
        if($response_server == '200'){
            $data_result = $this->verify_m->update_code($email, $random_number , $count_result);
            $response = 'check your email for verification code';
            $this->response($response, 200);
        }
       // }
    }

    //Function to connect with pringit api//
    public function httpGetWithErros($url) {
        $ch = curl_init();
       $link = trim($url);
        //$link = str_replace ( ' ', '%20', $link);
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        if ($output === false) {
            echo "Error Number:" . curl_errno($ch) . "<br>";
            echo "Error String:" . curl_error($ch);
            $output = "internet problem";
            return $output;
            //  exit();
        }
        curl_close($ch);

        return $output;
    }

        // forget password functionality
    function sendMail($email , $random_number){

    //call model to update data in db
   if(!empty($email)){
      $message = "Your verification code is ". $random_number . ' Thanx for using miniapp';
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "testonebyte@gmail.com";  // user email address
        $mail->Password   = "1byte@biz";            // password in GMail
        $mail->SetFrom('testonebyte@gmail.com', 'Miniapp');  //Who is sending the email
        //$mail->AddReplyTo("response@yourdomain.com","Firstname Lastname");  //email address that receives the response
        $mail->Subject    = "Miniapp Verification Code";
        $mail->Body      =  $message;
        $mail->AltBody    = "Plain text message"; 
        $destino = $email; // Who is addressed the email to
        $mail->AddAddress($destino, "John Doe");

      //  $mail->AddAttachment("images/phpmailer.gif");      // some attached files
      //  $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $msg = "Error: " . $mail->ErrorInfo;
        } else {
            $msg = "200";
        }

      return $msg;
     }


    } 

}
