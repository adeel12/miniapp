<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
        $this->load->library('session');
	}
    public function index() {
        
        $this->load->view('login');
    }

    public function test() {
        
        $this->load->view('welcome_message');
    }
    
    public function signin() {
        $this->load->model('login_m');
        $data = $this->input->post();
        $object = (object) $data;
        $result_data = $this->login_m->onlineSignin($object);
        if($result_data == 1){
            $session_data = $this->login_m->get_user_data($object);
            $this->session->set_userdata($session_data);
            $this->load->view('upload_videos');
        }else{
            
            $data['invalidMsg'] = 'Email or Password is incorrect';
            $this->load->view("login", $data);
        }
    
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/login.php */