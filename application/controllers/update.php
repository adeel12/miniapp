<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('update_m');
        $this->load->model('videos_m');
    }

    public function index() {
      
        $session_id = $this->session->all_userdata();
        $user_id = $session_id[0]->user_id;
        $data['update_data'] = $this->update_m->get_userdata($user_id);
        $this->load->view('update',$data);
        
    }
    
    public function view_profile() {

        $session_id = $this->session->all_userdata();
        $user_id = $session_id[0]->user_id;
        $data['update_data'] = $this->update_m->get_userdata($user_id);
        $this->load->view('view_profile', $data);
    }

    public function update_data() {

        $username = $_POST['username'];
        $profile_image = $_FILES['userfile']['name'];
        
        if($_FILES['userfile']['name'] != ''){
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'gif|jpg|png';
       // $config['max_size'] = '10000';
//        $config['max_width'] = '1024';
//        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $result_data = $this->update_m->update_data($username , $profile_image);
            $session_id = $this->session->all_userdata();
            $user_id = $session_id[0]->user_id;
            $data['update_data'] = $this->update_m->get_userdata($user_id);
            $this->load->view('view_profile',$data);
        }

      
        //$this->load->view('update', $data);
        }
    }

    public function listVideos(){

        $data['videos_data'] = $this->videos_m->get_videos();
        $this->load->view('list_videos', $data);
    }

    public function delete(){
        $id = $this->input->post();
        $video_id = $id['rule_id'];
        $res = $this->videos_m->delete_videos($video_id);
        print_r($res);
    }

    public function edit() {
        $video_id = $this->uri->segment(3);
        $data['video_id'] = $video_id;
        $this->load->view('edit_video',$data);
        
    }

    public function signout(){
        $this->session->sess_destroy();
        $this->load->view('login');

    }
    public function deleteall(){
        $id = $this->input->post();
        $video_ids = $id['rule_id'];
        $deleted = $this->videos_m->delete_all($video_ids);
        print_r($deleted);
    }    
}

?>