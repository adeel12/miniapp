<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('videos_m');
    }

    function index() {
        $this->load->view('upload_form', array('error' => ' '));
    }

    public function do_upload() {

        $videoName = $this->input->post('name');
        $session_id = $this->session->all_userdata(); 
        $id = $session_id[0]->user_id;
        if($id == null || $id == ''){
            echo '<script language="javascript">alert("You are not allowed to continue. Please signin")</script>';   
            redirect('/login/index', 'refresh'); 
        }
        $name = $session_id[0]->name;
        $images = array("1", "2");
        $this->load->library('upload');
        $last_id = $this->videos_m->get_last_id();
        
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
        if($_FILES['files1']['name'] == ""){
            echo '<script language="javascript">alert("Image is missing")</script>';        
            redirect('/upload/home/', 'refresh');
        }
        if($_FILES['files2']['name'] == ""){
            echo '<script language="javascript">alert("video is missing")</script>';        
            redirect('/upload/home/', 'refresh');
        }

        if(isset($_FILES['files1'])){
        if($_FILES['files1']['name'] != '' ) {
            
            $file_name = $_FILES['files1']['name'];
            $exceptions = array(" ", "-", ",", "'", "#", "?" ,"&");
            $imageName = str_replace($exceptions, "", $file_name);
            $fileName = explode('.', $imageName);
            // $uniqueFIlename =  $fileName[0].'_'.$random_number.$last_id.'.'.$fileName[1];
            $uniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1];            
            $file_type = $_FILES['files1']['type'];
            $file_size = $_FILES['files1']['size'];
            $result_data = $this->videos_m->insert_image($id, $name, $uniqueFIlename, $file_type);
        }
   
        }
        if(isset($_FILES['files2'])){
        if($_FILES['files2'] != '' && $result_data != '') {
            $file_name = $_FILES['files2']['name'];
            $exceptions = array(" ", "-", ",", "'", "#", "?" ,"&");
            $filename = str_replace($exceptions, "", $file_name);
            $fileName = explode('.', $filename);
            $newName = explode("_",$filename);
            // $uniqueFIlename =  $newName[0].'_'.$random_number.$last_id.'.'.$fileName[1];
            $uniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1];              
            $file_type = $_FILES['files2']['type'];
            $file_size = $_FILES['files2']['size'];
            $this->videos_m->insert_video($id, $name, $uniqueFIlename, $result_data , $videoName);
        }
        }

        foreach ($images as $i) {

            if (!empty($_FILES['files' . $i]['name'])) {
                $image_name = $_FILES['files' . $i]['name'];
                $file_type = $_FILES['files' . $i]['type'];
                $mediaType = explode("/", $file_type);

                if($mediaType[0] == "image"){
                    $exceptionsi = array(" ", "-", ",", "'", "#", "?" ,"&");
                    $imageNamei = str_replace($exceptions, "", $image_name);
                    $fileNamei = explode('.', $imageNamei);
                    // $imgUniqueFIlenamei =  $fileNamei[0].'_'.$random_number.$last_id.'.'.$fileNamei[1];   
                    $imgUniqueFIlenamei =  '_'.$random_number.$last_id.'.'.$fileNamei[1];  
                    $config['file_name'] = $imgUniqueFIlenamei;
                    $config['upload_path'] = './upload/';
                    $config['allowed_types'] = 'gif|jpg|png|mp4';

                    $this->upload->initialize($config);

                    $name = "files" . $i;
                    if (!$this->upload->do_upload($name)) {

                        // $data['videos_data'] = $this->videos_m->get_video_data($id);
                        // $this->load->view('list_videos', $data);
                    }                    
                }
                if($mediaType[0] == "video"){
                    $exceptions = array(" ", "-", ",", "'", "#", "?" ,"&");
                    $imageName = str_replace($exceptions, "", $image_name);
                    $fileName = explode('.', $imageName);
                    $newName = explode("_",$imageName);
                    // $videoUniqueFIlename =  $newName[0].'_'.$random_number.$last_id.'.'.$fileName[1];
                    $videoUniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1];  
                    $config['file_name'] = $videoUniqueFIlename;
                    $config['upload_path'] = './upload/';
                    $config['allowed_types'] = 'gif|jpg|png|mp4';

                    $this->upload->initialize($config);

                    $name = "files" . $i;
                    if (!$this->upload->do_upload($name)) {

                        // $data['videos_data'] = $this->videos_m->get_video_data($id);
                        // $this->load->view('list_videos', $data);
                    }
                }

            }
        }

         //code for sendng push notification
        $gcm_user_data = $this->videos_m->online_get_gcm_users($id);
        define("GOOGLE_API_KEY", "AIzaSyBhMBMLm17R9mPBQ1Fyzz38CNcEG6qi9YM");
        foreach ($gcm_user_data as $value) {
            $registatoin_ids = $value->gcm_regid;
            $pushMessage = 'testing';
            $gcmRegIds = array("rid" => $registatoin_ids);
            $message = array("m" => $pushMessage);
            $response_server = $this->sendPushNotificationToGCM($registatoin_ids, $message);
            $registatoin_ids = ''; 
        }
        redirect('/upload/videos/', 'refresh');
    }

    public function videos(){
        $data['videos_data'] = $this->videos_m->get_video_admin();
        $this->load->view('list_videos', $data);
    }

    public function home(){
        $this->load->view('upload_videos');
    }   
    //generic php function to send GCM push notification
     function sendPushNotificationToGCM($registatoin_ids, $message) {
		//Google cloud messaging GCM-API url
        $url =  'https://android.googleapis.com/gcm/send';
        
        
        $fields = array(
                'registration_ids'  => array($registatoin_ids),
                'data'              => array( "message" => $message ),
                );
        
		// Google Cloud Messaging GCM API Key
				
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }


    public function update() {
        $videoName = $this->input->post('name');
        $id = $this->input->post('video_id');
        $session_id = $this->session->all_userdata();
        // $id = $session_id[0]->user_id;
        $name = $session_id[0]->name;
        if($name == null || $name == ''){
            echo '<script language="javascript">alert("You are not allowed to continue. Please signin")</script>';   
            redirect('/login/signin/', 'refresh'); 
        }
        $images = array("1", "2");
        $this->load->library('upload');
         $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
        if($_FILES['files1']['name'] == ""){
            echo '<script language="javascript">alert("Image is missing")</script>';        
            redirect('/upload/home/', 'refresh');
        }
        if($_FILES['files2']['name'] == ""){
            echo '<script language="javascript">alert("video is missing")</script>';        
            redirect('/upload/home/', 'refresh');
        }

        if(isset($_FILES['files1'])){
        if($_FILES['files1']['name'] != '' ) {
            
            $file_name = $_FILES['files1']['name'];
            $exceptions = array(" ", "-", ",", "'", "#", "?" );
            $imageName = str_replace($exceptions, "", $file_name);
            $fileName = explode('.', $imageName);
            // $uniqueFIlename =  $fileName[0].'_'.$random_number.$last_id.'.'.$fileName[1];
            $uniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1]; 
            $file_type = $_FILES['files1']['type'];
            $file_size = $_FILES['files1']['size'];
            $result_data = $this->videos_m->insert_image($id, $name, $uniqueFIlename, $file_type);
        }
   
        }
        if(isset($_FILES['files2'])){
        if($_FILES['files2'] != '' && $result_data != '') {
            $file_name = $_FILES['files2']['name'];
            $exceptions = array(" ", "-", ",", "'", "#", "?");
            $filename = str_replace($exceptions, "", $file_name);
            $fileName = explode('.', $filename);
            $newName = explode("_",$filename);
            // $uniqueFIlename =  $newName[0].'_'.$random_number.$last_id.'.'.$fileName[1];
            $uniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1];             
            $file_type = $_FILES['files2']['type'];
            $file_size = $_FILES['files2']['size'];
            $this->videos_m->insert_video($id, $name, $uniqueFIlename, $result_data , $videoName);
        }
        }

        foreach ($images as $i) {

            if (!empty($_FILES['files' . $i]['name'])) {
                $image_name = $_FILES['files' . $i]['name'];
                $file_type = $_FILES['files' . $i]['type'];
                $mediaType = explode("/", $file_type);

                if($mediaType[0] == "image"){
                    $exceptionsi = array(" ", "-", ",", "'", "#", "?");
                    $imageNamei = str_replace($exceptions, "", $image_name);
                    $fileNamei = explode('.', $imageNamei);
                    // $imgUniqueFIlenamei =  $fileNamei[0].'_'.$random_number.$last_id.'.'.$fileNamei[1];   
                    $imgUniqueFIlenamei =  '_'.$random_number.$last_id.'.'.$fileNamei[1]; 
                    $config['file_name'] = $imgUniqueFIlenamei;
                    $config['upload_path'] = './upload/';
                    $config['allowed_types'] = 'gif|jpg|png|mp4';

                    $this->upload->initialize($config);

                    $name = "files" . $i;
                    if (!$this->upload->do_upload($name)) { 

                        // $data['videos_data'] = $this->videos_m->get_video_data($id);
                        // $this->load->view('list_videos', $data);
                    }                    
                }
                if($mediaType[0] == "video"){
                    $exceptions = array(" ", "-", ",", "'", "#", "?");
                    $imageName = str_replace($exceptions, "", $image_name);
                    $fileName = explode('.', $imageName);
                    $newName = explode("_",$imageName);
                    // $videoUniqueFIlename =  $newName[0].'_'.$random_number.$last_id.'.'.$fileName[1]; 
                    $videoUniqueFIlename =  '_'.$random_number.$last_id.'.'.$fileName[1]; 
                    $config['file_name'] = $videoUniqueFIlename;
                    $config['upload_path'] = './upload/';
                    $config['allowed_types'] = 'gif|jpg|png|mp4';

                    $this->upload->initialize($config);

                    $name = "files" . $i;
                    if (!$this->upload->do_upload($name)) {

                        // $data['videos_data'] = $this->videos_m->get_video_data($id);
                        // $this->load->view('list_videos', $data);
                    }
                }
                // $exceptions = array(" ", "-", ",", "'", "#", "?");
                // $imageName = str_replace($exceptions, "", $image_name);
                // $fileName = explode('.', $imageName);
                // $newName = explode("_",$imageName);
                // $uniqueFIlename =  $newName[0].'_'.$random_number.$last_id.'.'.$fileName[1]; 

                // $config['upload_path'] = './upload/';
                // $config['allowed_types'] = 'gif|jpg|png|mp4';

                // $this->upload->initialize($config);

                // $name = "files" . $i;
                // if (!$this->upload->do_upload($name)) {

                //     // $data['videos_data'] = $this->videos_m->get_video_data($id);
                //     // $this->load->view('list_videos', $data);
                // }
            }
        }
        $this->session->unset_userdata('video_id');
        redirect('/upload/videos/', 'refresh');
    }


}

?>
