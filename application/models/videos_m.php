<?php

class Videos_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function video_share($data) {
        //increment beam of related shared video

        $v_id = $data->v_id;
        //print_r($v_id);
        $this->db->where('ID', $data->v_id);
        $query = $this->db->get('video_repository');
        $result_data = $query->result();
       // $video_id = $result_data[0]->id_server_video;
        $like_count = $result_data[0]->beam;
        $like_count +=1;

        $update_data = array(
            'beam' => $like_count
        );
        $this->db->where('ID', $v_id);
        $this->db->update('video_repository', $update_data);


        //get data of mini users
        $this->db->select('user_id, u_mobile_no , name ,');
        $this->db->from('users');
        $this->db->join('imported_contacts', 'imported_contacts.imported_num = users.u_mobile_no');
        $this->db->group_by("users.u_mobile_no");
        $query_mini_user = $this->db->get();
        $mini_users = $query_mini_user->result();

        //insert data for mini user to liked table
        foreach ($mini_users as $mini) {
            $current_time = date("Y-m-d H:i:s");
            $mini_data = array(
                'video_id' => $v_id,
                'shared_by' => $data->number,
                'shared_to' => $mini->u_mobile_no,
                'created_at' => $current_time
            );
            $this->db->insert('videos_liked', $mini_data);
        }


        //get data of non-mini users
        $this->db->where('imported_contacts.imported_num !=', $data->number);
        $this->db->where('imported_contacts.importer_num', $data->number);
        $this->db->select('imported_contacts.imported_num as number');
        //$this->db->group_by("users.u_mobile_no");
        $query_non_mini = $this->db->get('imported_contacts');
        $query_result = $query_non_mini->result();



        return $query_result;
    }

    function insert_image($id, $name, $file_name, $file_type) {


        $splash_url = base_url() . 'upload/' . $file_name;
        $video_data = array(
            'uploader_id' => $id,
            'id_server_video' => $id,
            'uploader_name' => $name,
            'splash_url' => $splash_url
        );
        $return_data = $this->db->insert('ma_video_repository', $video_data);

        return $this->db->insert_id();
    }


    function update_image($id, $name, $file_name, $file_type) {


        $splash_url = base_url() . 'upload/' . $file_name;
        $video_data = array(
            'splash_url' => $splash_url
        );
        // $return_data = $this->db->insert('ma_video_repository', $video_data);
        $this->db->where('ID', $id);
        $this->db->update('ma_video_repository', $video_data);        

        return $id;
    }    

    function insert_video($id, $name, $file_name, $last_id , $videoName) {


        $splash_url = base_url() . 'upload/' . $file_name;
        $video_data = array(
            'uploader_id' => $id,
            'id_server_video' => $id,
            'uploader_name' => $name,
            'video_url' => $splash_url,
            'video_name' => $videoName
        );
        $this->db->where('ID', $last_id);
        $this->db->update('ma_video_repository', $video_data);

        return $this->db->insert_id();
    }

    function update_video($id, $videoname, $file_name, $last_id, $name) {


        $splash_url = base_url() . 'upload/' . $file_name;
        $video_data = array(
            'uploader_id' => $id,
            'id_server_video' => $id,
            'uploader_name' => $name,
            'video_url' => $splash_url,
            'video_name' => $videoname
        );
        $this->db->where('ID', $id);
        $this->db->update('ma_video_repository', $video_data);

        return $id;
    }    

    function get_video_data($id) {

        $this->db->where('uploader_id', $id);
        $viseos = $this->db->get('video_repository');
        $data_list = $viseos->result();
        return $data_list;
    }

    function get_video_admin() {
        $viseos = $this->db->get('video_repository');
        $data_list = $viseos->result();
        return $data_list;
    }    

    function get_gcm_users($data) {


        // $v_id = $data->v_id;
        // $this->db->where('video_id', $data->v_id);
        // $query = $this->db->get('videos_liked');
        // $query_result = $query->result();
        $v_id = $data->v_id;
        //print_r($v_id);
        $this->db->where('ID', $data->v_id);
        $query = $this->db->get('video_repository');
        $result_data = $query->result();
       // $video_id = $result_data[0]->id_server_video;
        $like_count = $result_data[0]->beam;
        $like_count +=1;

        $update_data = array(
            'beam' => $like_count
        );
        $this->db->where('ID', $v_id);
        $this->db->update('video_repository', $update_data);        


        $this->db->where('imported_contacts.importer_num', $data->number);
        $this->db->select('imported_contacts.imported_num');
        $query_share = $this->db->get('imported_contacts');
        $share_result = $query_share->result();
        $test = $this->db->last_query();
       // print_r($test);
       // exit;

        foreach ($share_result as $value) {

            $this->db->where('u_mobile_no ', $value->imported_num);
            $this->db->select('user_id');
            $gcm_query = $this->db->get('users');
            $res = $gcm_query->result();
            // print_r($res);
            if (isset($res[0]->user_id)) {
                $this->db->where('gcm_user_id ', $res[0]->user_id);
                $this->db->select('gcm_regid');
                $gcm_query2 = $this->db->get('gcm_users');
                $result[] = $gcm_query2->result();
            }
        }
        $oneDimensional = call_user_func_array('array_merge', $result);
        return $oneDimensional;
    }

    function online_get_gcm_users($id) {

        $this->db->where('gcm_user_id !=', $id);
        $this->db->select('gcm_regid');
        $query = $this->db->get('gcm_users');
        $online_result = $query->result();
        return $online_result;
    }

    function get_videos(){
        $viseos = $this->db->get('video_repository');
        $data_list = $viseos->result();
        return $data_list;            
    }

    function delete_videos($video_id){

        $this->db->where('ID', $video_id);
        $res = $this->db->delete('video_repository');
        return $res;
    }

    function get_last_id(){
        $this->db->limit(1);
        $this->db->order_by("ID", "desc");
        $videos = $this->db->get('video_repository');
        $result = $videos->result();
        if(empty($result)){
            $last_id = '1';
        }else{
            $last_id = $result[0]->ID;
        }
        return $last_id;        
    } 


    public function delete_all($video_ids){

        $names = $video_ids;
        $this->db->where_in('ID', $names);
        $this->db->delete('video_repository');

        return $res;
    }    

}
