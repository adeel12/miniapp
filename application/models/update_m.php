<?php

class Update_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_userdata($user_id) {
       
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('users');
        $res = $result->result();
        
        
        return $res;
    }
    
    public function update_data($username , $profile_image) {
        $session_id = $this->session->all_userdata();
        $user_id = $session_id[0]->user_id;
        $data = array(
                       'name' => $username,
                       'profile_image' => $profile_image
                    );

        $this->db->where('user_id', $user_id);
        $res = $this->db->update('users', $data); 

        return $res;
    }

}
