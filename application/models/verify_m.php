<?php

class Verify_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function number_exists($email) {
        $query = $this->db->where('email', $email);
        $query = $this->db->get('verify_number');
        $res = $query->num_rows();
        return $res;
    }

    function insert_code($email, $verification_code ) {
        $current_time = date("Y-m-d H:i:s");
        $data = array(
            'verify_code' => $verification_code,
            'email' => $email,
            'user_type' => 'mobile',
            'created_at' => $current_time
        );

        $return_data = $this->db->insert('verify_number', $data);
        return $return_data;
    }

    function update_code($email, $verification_code, $trigger_value  ) {
        $query = $this->db->where('email', $email);
        $query = $this->db->get('verify_number');
        $res = $query->result();
        $verify_num_id = $res[0]->v_id;
        $trigger_count = $res[0]->trigger_count;

        if ($trigger_count > 3) {

            return $trigger_count;
        } else {


            $trigger_count +=1;
            $current_time = date("Y-m-d H:i:s");
            $data = array(
                'verify_code' => $verification_code,
                'email' => $email,
                'trigger_count' => $trigger_count,
                'created_at' => $current_time
            );

            $this->db->where('v_id', $verify_num_id);
            $result = $this->db->update('verify_number', $data);
            return $result;
        }
    }

    function get_count($email) {
        $this->db->where('email', $email);
        $get_count = $this->db->get('verify_number');
        $res = $get_count->result();
        $count = $res[0]->trigger_count;
        return $count;
    }
    
    function insert_code_online($mobile_num, $verification_code) {
        $current_time = date("Y-m-d H:i:s");
        $data = array(
            'verify_code' => $verification_code,
            'v_mobile_num' => $mobile_num,
            'user_type' => 'online',
            'created_at' => $current_time
        );

        $this->db->insert('verify_number', $data);
        
        $this->db->where('v_mobile_num', $mobile_num);
        $this->db->where('verify_code', $verification_code);
        $result = $this->db->get('verify_number');
        $res = $result->result();
        
        
        return $res;
    }
    
    function get_code_online($mobile_num) {
       
        $this->db->where('v_mobile_num', $mobile_num);
        $result = $this->db->get('verify_number');
        $res = $result->result();
        
        
        return $res;
    }   
    
  

}
