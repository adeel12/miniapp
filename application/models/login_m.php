<?php

class Login_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function number_exists($num) {
        $query = $this->db->where('v_mobile_num', $num);
        $query = $this->db->get('verify_number');
        $res = $query->num_rows();
        return $res;
    }

    function email_exists($num) {
        $query = $this->db->where('email', $num->email);
        $query = $this->db->get('users');
        $res = $query->num_rows();
        return $res;
    }

    function check_code($num) {
        $this->db->where('email', $num->email);
        $this->db->where('verify_code', $num->verify_code);
        $query = $this->db->get('verify_number');
        $res = $query->num_rows();
        return $res;
    }

    function register($alldata) {
        $mob_num = $alldata->mob_numb;
        $email = $alldata->email;
        $verify_code = $alldata->verify_code;
        $user_name = $alldata->user_name;
        $user_pwd = $alldata->user_pwd;
        $gcm_regid = $alldata->gcm_regid;

        $query = $this->db->where('u_mobile_no', $mob_num);
        $query = $this->db->get('users');
        $count = $query->num_rows();

       // if ($count < 1) {
            $current_time = date("Y-m-d H:i:s");
            $user_data = array(
                'u_mobile_no' => $mob_num,
                'verify_code' => $verify_code,
                'name' => $user_name,
                'password' => $user_pwd,
                'email' => $email,
                'crated_at' => $current_time
            );
            $return_data = $this->db->insert('users', $user_data);
            $last_id = $this->db->insert_id();

            $gcm_data = array(
                'gcm_regid' => $gcm_regid,
                'gcm_user_id' => $last_id,
                'created_at' => $current_time
            );
            $this->db->insert('gcm_users', $gcm_data);
            return $return_data;
        // } else {
        //     return false;
        // }
    }

    function onlineSignin($signin_data) {

        $this->db->where('email', $signin_data->email);
        $this->db->where('password', $signin_data->password);
        $query = $this->db->get('users');
        $result = $query->result();
        $count = $query->num_rows();
        if ($count == 1) {

            return 1;
        } else {
            return 0;
        }
    }

    function signin($signin_data) {
        $this->db->where('email', $signin_data->email);
        $this->db->where('password', $signin_data->password);
        $query = $this->db->get('users');
        $result = $query->result();
        $count = $query->num_rows();

        if ($count == 1) {
        $this->db->where('gcm_user_id', $result[0]->user_id);
        $gcm = $this->db->get('gcm_users');
        $gcm_user = $gcm->result();

        if (isset($gcm_user[0]->gcm_id)) {
            $gcm_id = $gcm_user[0]->gcm_id;
        } else {
            $gcm_id = '';
        }
        if ($gcm_id == '') {
            $current_time = date("Y-m-d H:i:s");
            $user_data = array(
                'gcm_regid' => $signin_data->gcm_regid,
                'gcm_user_id' => $result[0]->user_id,
                'created_at' => $current_time
            );
            $this->db->insert('gcm_users', $user_data);
        } else {
            $current_time = date("Y-m-d H:i:s");
            $update_data = array(
                'gcm_regid' => $signin_data->gcm_regid,
                'created_at' => $current_time
            );

            $this->db->where('gcm_id', $gcm_id);
            $this->db->update('gcm_users', $update_data);
        }

        $this->db->where('email', $signin_data->email);
        $this->db->where('password', $signin_data->password);
        $res_data= $this->db->get('users');
        $result = $res_data->result();
            return $result;
        } else {
            return 0;
        }
    }

    function update_user($data) {

        $this->db->where('u_mobile_no', $data['number']);
        $this->db->where('name', $data['name']);
        $query = $this->db->get('users');
        $result_data = $query->result();
        $user_id = $result_data[0]->user_id;



        $update_data = array(
            'u_mobile_no' => $data['number'],
            'name' => $data['name']
        );

        $this->db->where('user_id', $user_id);
        $this->db->update('users', $update_data);
    }

    function register_online_user($userdata) {

        $username = $userdata['username'];
        $email = $userdata['email'];
        $firstname = $userdata['firstname'];
        $lastname = $userdata['lastname'];
        $user_pwd = $userdata['password'];

        $query = $this->db->where('email', $email);
        $query = $this->db->or_where('name', $username);
        $query = $this->db->get('users');
        $count = $query->num_rows();
        if ($count < 1) {
            $current_time = date("Y-m-d H:i:s");
            $user_data = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'name' => $username,
                'password' => $user_pwd,
                'user_type' => 'online',
                'crated_at' => $current_time
            );
            $return_data = $this->db->insert('users', $user_data);
            return $return_data;
        } else {
            return false;
        }
    }

    function get_user_data($data) {

        $this->db->where('email', $data->email);
        $this->db->select('user_id , u_mobile_no , name ,  user_type');
        $query = $this->db->get('users');
        $result_data = $query->result();
        return $result_data;
    }

}
