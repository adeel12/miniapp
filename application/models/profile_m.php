<?php

class Profile_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function update_user($cover_image , $profile_data) {

        if (!empty($profile_data->email)) {
        if(!empty($profile_data)){
        $cover_img =$profile_data->cover_image;
        $profile_img =$profile_data->image;  
        $name = $profile_data->name;
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
            $image_name = 'img_p_';
            $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/miniapp/upload/' . $image_name.'_'.$random_number.'.png';
            $data= $profile_img; 
            $data = base64_decode($data);
            file_put_contents($destination_folder, $data);
            $profile_image = base_url().'upload/'.$image_name.'_'.$random_number.'.png'; 
            $update_data = array(
                'profile_image' => $profile_image, 
                'cover_image' => $cover_image,
                'name' => $name
            );
            $this->db->where('email', $profile_data->email);
            $updateQuery = $this->db->update('users', $update_data);
            return $updateQuery;
            }

        } else {

            return 'false';
        }
    }


    function update_profile_image($profile_data) {

        if (!empty($profile_data->email)) {
        if(!empty($profile_data)){
        $profile_img =$profile_data->image;  
        $name = $profile_data->name;
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
            $image_name = 'img_p_';
            $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/miniapp/upload/' . $image_name.'_'.$random_number.'.png';
            $data= $profile_img; 
            $data = base64_decode($data);
            file_put_contents($destination_folder, $data, 100);
            $profile_image = base_url().'upload/'.$image_name.'_'.$random_number.'.png'; 
            $update_data = array(
                'profile_image' => $profile_image,
                'name' => $name
            );
            $this->db->where('email', $profile_data->email);
            $updateQuery = $this->db->update('users', $update_data);
            return $updateQuery;
            }

        } else {

            return 'false';
        }
    } 

    function update_cover_image($profile_data) {

        if (!empty($profile_data->email)) {
        if(!empty($profile_data)){
        $cover_img =$profile_data->cover_image;  
        $name = $profile_data->name;
        $digits = 4;
        $random_number = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
            $image_name = 'img_p_';
            $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/miniapp/upload/' . $image_name.'_'.$random_number.'.png';
            $data= $cover_img; 
            $data = base64_decode($data);
            file_put_contents($destination_folder, $data);
            $cover_image = base_url().'upload/'.$image_name.'_'.$random_number.'.png'; 
            $update_data = array(
                'cover_image' => $cover_image, 
                'name' => $name
            );
            $this->db->where('email', $profile_data->email);
            $updateQuery = $this->db->update('users', $update_data);
            return $updateQuery;
            }

        } else {

            return 'false';
        }
    }


    function update($profile_data) {

        if (!empty($profile_data->email)) {
        if(!empty($profile_data)){
        $name = $profile_data->name;
            $update_data = array( 
                'name' => $name
            );
            $this->db->where('email', $profile_data->email);
            $updateQuery = $this->db->update('users', $update_data);
            return $updateQuery;
            }

        } else {

            return 'false';
        }
    }        

    //function for importing contact list
    function contacts_list($contacts_list) {


        foreach ($contacts_list as $key => $value) {

            if ($value->ID) {

                // print_r($value->number);
                $code = $value->number;
                //print_r($code[1]); 
                if ($code[0] != '9' && $code[1] != '2') {
                    $str = $value->number;
                    $str = preg_replace('/^./', '+92', $str);
                    $str_number = preg_replace('/\s+/', '', $str);
                } else {
                    $str = $value->number;
                    $str_number = preg_replace("/92/", '+92', $str);
                }


            $query = $this->db->query('SELECT * FROM ma_imported_contacts ORDER BY  `contact_id` DESC LIMIT 1');
            $res = $query->result();
            if(isset($res[0])){
                $pic_id = $res[0]->contact_id;
            }else{
                $pic_id = '0';
            }

                       //function to create image from base64
                       // $image_name = 'image_'.$pic_id;
                       // $destination_folder = $_SERVER['DOCUMENT_ROOT'] . '/miniapp/upload/' . $image_name . '.png';
                       // $data= $value->base64; 
                       // $data = base64_decode($data);
                       // file_put_contents($destination_folder, $data);
                       // $image_url = base_url() . 'upload/' . $image_name . '.png';

                $current_time = date("Y-m-d H:i:s");
                $base64 = $value->base64;
                //$image = base64_decode($base64);
                $contact_data = array(
                    'importer_num' => $value->ID,
                    'imported_num' => $str_number,
                    'contact_name' => $value->name,
                    'contact_image' => $value->base64,
                    'created_at' => $current_time
                );
                $this->db->insert('imported_contacts', $contact_data);
            }
        }
        //  }
//        $allContents = '';
//        foreach ($contacts_list as $key => $getContacts) {
//
//            $str = $getContacts->number;
//            $str = preg_replace('/^./', '+92', $str);
//            $number = preg_replace('/\s+/', '', $str);
//
//            $this->db->where('users.u_mobile_no !=', $value->ID);
//            $this->db->where('users.u_mobile_no', $number);
//            $this->db->select('users.u_mobile_no as number');
//            $this->db->group_by("users.u_mobile_no");
//            $query = $this->db->get('users');
//            // $test = $this->db->last_query();
//            $contacts = $query->result();
//
//            foreach ($contacts as $val) {
//                $allContents .= $val->number . ", ";
//            }
//
////$test = $contacts[$key];
//        }
        return 'true';
    }

    function insert_contact_list($contacts_list) {

        foreach ($contacts_list as $value) {

            if ($value->ID) {

                $str = $value->number;
                $str = preg_replace('/^./', '+92', $str);
                $str_number = preg_replace('/\s+/', '', $str);

                $current_time = date("Y-m-d H:i:s");
                $base64 = $value->base64;
                //$image = base64_decode($base64);
                $contact_data = array(
                    'importer_num' => $value->ID,
                    'imported_num' => $str_number,
                    'contact_name' => $value->name,
                    'contact_image' => $base64,
                    'created_at' => $current_time
                );
                $this->db->insert('imported_contacts', $contact_data);
            }
        }
        $allContents = '';
        foreach ($contacts_list as $key => $getContacts) {

            $str = $getContacts->number;
            $str = preg_replace('/^./', '+92', $str);
            $number = preg_replace('/\s+/', '', $str);

            $this->db->where('users.email !=', $value->ID);
            $this->db->where('users.email', $number);
            $this->db->select('users.email as number');
            $this->db->group_by("users.email");
            $query = $this->db->get('users');
            // $test = $this->db->last_query();
            $contacts = $query->result();

            foreach ($contacts as $val) {
                $allContents .= $val->number . ", ";
            }
        }
        return $allContents;
    }

    function allvideos($value) {
        
        //get login user uploaded videso
        $this->db->where('users.u_mobile_no', $value);
        $this->db->select('ID as video_id , uploader_name as name_user , video_url as video , splash_url as splash , video_name as name_video , beam , splash_url as userpic');
        $this->db->join('users', 'users.user_id = video_repository.uploader_id');
        $query = $this->db->get('video_repository');
        $user_uploaded_videos = $query->result();
        if(!empty($user_uploaded_videos)){
        //get login user friends videos
        $this->db->where('imported_num', $value);
        $this->db->select('imported_contacts.importer_num ');
        $query = $this->db->get('imported_contacts');
        $friend_videos_list = $query->result();

        if($friend_videos_list != '' || isset($friend_videos_list) ){ 
        foreach ($friend_videos_list as $get_friends_videos) {
            $this->db->where('users.u_mobile_no', $get_friends_videos->importer_num);
            $this->db->select('ID as video_id , uploader_name as name_user , video_url as video , splash_url as splash , video_name as name_video , beam , splash_url as userpic');
            $this->db->join('users', 'users.user_id = video_repository.uploader_id');
            $query = $this->db->get('video_repository');
            $user_uploaded_videos_list[] = $query->result();
        
        //covert multidimentional array to onedimentional array
        $friend_videos_data1 = call_user_func_array('array_merge', $user_uploaded_videos_list);
        }
        
        }else{
            $friend_videos_data1 = '';
        }
        
        
        $this->db->where('importer_num', $value);
        $this->db->select('imported_contacts.imported_num , users.user_id');
        $this->db->join('users', 'users.u_mobile_no = imported_contacts.imported_num');
        $query = $this->db->get('imported_contacts');
        $friend_beam = $query->result();
        if(!empty($friend_beam)){
        foreach ($friend_beam as $val) {
            $this->db->where('video_repository.uploader_id', $val->user_id);
            $this->db->group_by("ID");
            $this->db->select('ID as video_id , uploader_name as name_user , video_url as video , splash_url as splash , video_name as name_video , beam , splash_url as userpic');
            $videos = $this->db->get('video_repository');
            $video[] = $videos->result();
        }
        
        $friend_videos_data = call_user_func_array('array_merge', $video);
        }else{
            $friend_videos_data = '';
        }
        if(isset($friend_videos_data1) && $friend_videos_data != ''){
           // exit('1');
        //convert multidimentinal array to one dimentional array
                           // $videos_result = array_merge($friend_videos_data1, $friend_videos_data);
                           // print_r($videos_result);
                           // exit;
                             //convert object to array
                            $array2 = json_decode(json_encode($friend_videos_data),TRUE); 

                            //get unique values from array
                            $array = array_values(array_combine(array_map(function ($i) { return $i['video_id']; }, $array2), $array2));
                            //print_r($array);

                            
                            $resut_data = array_merge($user_uploaded_videos, $array);
                            
                            $all_data = array_merge($resut_data, $user_uploaded_videos);
                            
                            return $friend_videos_data;
        
        }else if(isset($friend_videos_data1) && $friend_videos_data1 != '' ){
             // exit('2');
                if($user_uploaded_videos){
                            $videos_result = array_merge($friend_videos_data1, $user_uploaded_videos);
                            
                            //convert object to array
                            $array2 = json_decode(json_encode($videos_result),TRUE); 
                            
                            //get unique values from array
                            $array = array_values(array_combine(array_map(function ($i) { return $i['video_id']; }, $array2), $array2));
                            //print_r($array)
                            return $array;
                }else{
                    
                    return $friend_videos_data1;
                }
        }else if(isset($friend_videos_data) && $friend_videos_data != ''){
             // exit('3');
             if($user_uploaded_videos){
                            $videos_result = array_merge($friend_videos_data, $user_uploaded_videos);
                            //convert object to array
                            $array2 = json_decode(json_encode($videos_result),TRUE); 
                            //get unique values from array
                            $array = array_values(array_combine(array_map(function ($i) { return $i['video_id']; }, $array2), $array2));
                            return $array;
                }else{
                    return $friend_videos_data;
                }    
        }else{ 
            return $user_uploaded_videos;
        }

    }else{

        $this->db->order_by("ID", "desc");
        $this->db->select('ID as video_id , uploader_name as name_user , video_url as video , splash_url as splash , video_name as name_video , beam , splash_url as userpic');
        $query = $this->db->get('video_repository');
        $default_uploaded_videos = $query->result();
        return $default_uploaded_videos;
    }
//        //convert object to array
//        $array2 = json_decode(json_encode($videos_result),TRUE); 
//        //get unique values from array
//        $array = array_values(array_combine(array_map(function ($i) { return $i['video_id']; }, $array2), $array2));
//        //print_r($array);
//        
//        
//        $resut_data = array_merge($user_uploaded_videos, $array);
//        //print_r($resut_data);
        

        //return $resut_data;


    }

    function getUserData($data) {

        $this->db->where('email', $data->email);
        $this->db->select('name , profile_image as user_image , cover_image');
        $profile_data = $this->db->get('users');
        $result = $profile_data->result();

        return $result;
    }

    function getMiniUser($number) {


//        $this->db->where('importer_num', $number);
//        $this->db->select('imported_contacts.imported_num ');
//        $query = $this->db->get('imported_contacts');
//        $result = $query->result();
//        $test = $this->db->last_query();
//       // $miniNumbers = '';
//        foreach ($result as $val) {
            
            $this->db->where('users.u_mobile_no !=', $number);
            $this->db->select('u_mobile_no as number, name , profile_image as image, user_id ');
            $query = $this->db->get('users');
            $result = $query->result();
//            if($result){
//
//            $miniNumbers[] = $result;
//            
//            }
//        }

        //$oneDimensionalArray = call_user_func_array('array_merge', $miniNumbers);
        return $result;
    }

    function getNonMiniUser($number) {

        $this->db->where('importer_num', $number);
        $this->db->select('imported_contacts.imported_num ');
        $this->db->select('imported_num as number, contact_name as name , contact_image as image ');
        $query = $this->db->get('imported_contacts');
        $result = $query->result_array();
        // return $result;
        // $oneDimensionalArray = array_map('current', $result);

        // $this->db->select('users.u_mobile_no ');
        // $query = $this->db->get('users');
        // $result2 = $query->result_array();
        // $oneDimensional = array_map('current', $result2);
        // $result3 = array_diff($oneDimensionalArray, $oneDimensional);
        
       
        
       
        // foreach ($result as $key => $val) {

        //     $this->db->where('imported_contacts.imported_num ', $val['imported_num']);
        //     $this->db->select('imported_num as number, contact_name as name , contact_image as image ');
        //     $query = $this->db->get('imported_contacts');
        //     $result = $query->result();
        //     if ($result) {

        //         $nonMiniNumbers[] = $result;
        //     }
        // }

        //  $oneDimensional= call_user_func_array('array_merge', $nonMiniNumbers);

        return $result;
    }    
	
	
	
	//get mini count of mini users
	function get_mini_count($number) {

       // $this->db->get('users');
        $count = $this->db->where('imported_contacts.importer_num ', $number);
        $total_count = $count->count_all_results('imported_contacts');
        return $total_count;
    }   

    //get password of current user
    function get_password($number) {

        $this->db->where('u_mobile_no', $number);
        $this->db->select('password , u_mobile_no as number , user_id');
        $query = $this->db->get('users');
        $result = $query->result();
        return $result[0];
    }

    function password($email) {

        $this->db->where('email', $email);
        $this->db->select('password , u_mobile_no as number , user_id');
        $query = $this->db->get('users');
        $result = $query->result();
        return $result[0];
    }

    function viewer_info($uid){
        $this->db->where('user_id', $uid);
        $this->db->select('u_mobile_no as number , name as username, profile_image , cover_image');
        $query = $this->db->get('users');
        $result = $query->result();   
        return $result;
    }

}
